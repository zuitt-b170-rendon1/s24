db.rooms.insert({
		    name: "single",
		    accomodates: 2,
		    price: 1000,
		    description: "A simple room with all the basic necessities",
		    rooms_available: 10,
		    isAvailable: false
		});
    
    // Inserting multiple documents
		db.rooms.insertMany([
		    {
		        name: "double",
		        accomodates: 3,
		        price: 2000,
		        description: "A room fit for a small family going on a vacation",
		        rooms_available: 5,
		        isAvailable: false
		    },
		    {
		        name: "queen",
		        accomodates: 4,
		        price: 4000,
		        description: "A room with a queen sized bed perfect for a simple getaway",
		        rooms_available: 15,
		        isAvailable: false
		    }
		]);
    
    
    // Find a document
		db.rooms.find({ name: "double" });
    
    
    // Updating a document
		db.rooms.updateOne(
		    { name: "queen" },
		    {
		        $set : {
		            rooms_available: 0
		        }
		    }
		);
    
    // Deleting multiple documents
		db.rooms.deleteMany({
		    rooms_available: 0
		})

		db.rooms.find()
    
    
    
#  Comparison Query Operators
# 	$gt/$gte operator

		db.collectionName.find({ field : { $gt : value } });
		db.collectionName.find({ field : { $gte : value } });
        
    db.users.find({ age: { $gt : 50 }})
        
# 	$lt/$lte operator
		db.users.find({ age : { $lt : 50 } }).pretty();
		db.users.find({ age : { $lte : 50 } }).pretty();


# 	$ne operator
		db.users.find({ age : { $ne: 82 } }).pretty();
    
# 	$in operator
    db.users.find( { lastName: { $in: [ "Hawking", "Doe" ] } } ).pretty();
		db.users.find( { courses: { $in: [ "HTML", "React" ] } } ).pretty();
    
#   Logical Query Operators

# 	$or operator
		db.users.find( { $or: [ { firstName: "Neil" }, { age: "25" } ] } ).pretty()
    
#   $and operator
		db.users.find({ $and: [ { age : { $ne: 82 } }, { age : { $ne: 76 } }] }).pretty();


#     Field Projection
# Retrieving documents are common operations that we do and by 
# default MongoDB queries return the whole document as a response.

# When dealing with complex data structures, there might 
# be instances when fields are not useful for the query that
# we are trying to accomplish. 

# To help with readability of the values returned, 
# we can include/exclude fields from the response.

# Inclusion
			- Allows us to include/add specific fields only when retrieving documents.
			- The value provided is 1 to denote that the field is being included.
			- Syntax
				db.users.find({criteria},{field: 1})
        
    db.users.find( 
			{ 
				firstName: "Jane" 
			}, 
			{ 
				firstName: 1, 
				lastName: 1,
			}
		)
    
    
    // Exclusion
		/*
			- Allows us to exclude/remove specific fields only when retrieving documents.
			- The value provided is 0 to denote that the field is being included.
			- Syntax
				db.users.find({criteria},{field: 1})
		*/
		db.users.find( 
			{ 
				firstName: "Jane" 
			}, 
			{ 
				contact: 0, 
				department: 0 
			} 
		).pretty();
    
    
    
    // Suppressing the ID field
		/*
			- Allows us to exclude the "_id" field when retrieving documents.
			- When using field projection, field inclusion and exclusion may not be used at the same time.
			- Excluding the "_id" field is the only exception to this rule.
			- Syntax
				db.users.find({criteria},{_id: 0})
		*/
		db.users.find( 
			{ 
				firstName: "Jane" 
			}, 
			{ 
				firstName: 1, 
				lastName: 1,
				contact: 1,
				_id: 0
			}
		).pretty();
    
   //  Evaluation Query Operators

		// $regex operator
		/*
			- Allows us to find documents that match a specific 
      	string pattern using regular expressions.
			- Syntax
				db.users.find({ field: $regex: 'pattern', $options: '$optionValue' });
		*/

		// Case sensitive query
		db.users.find({ firstName: { $regex: 'N' } })
    
    // Case insensitive query
		db.users.find({ firstName: { $regex: 'j', $options: '$i' } }).pretty();